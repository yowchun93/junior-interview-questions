# Junior/Mid Interview Questions
*Please include explanation along with your answers.*

1. Share with us one open source contribution or side project that you have worked on. What is it about? What have you learnt? What are the challenges? How did you overcome them?

2. Why are you interested in the blockchain industry?

3. Tell us about a newer (less than five years old) web technology you like and why?

4. How does HTTP protocol works?

5. You are helping your company to host a web application on Amazon Web Services. As part of designing the architecture to host the application, you have the option to deploy the application in the following manner:-
    * (A) Single availability zone
    * (B) Multiple availability zones in a single region
    * (C) Multi regions

    *Describe which option you would choose and why? Also explain why you did not choose the other options.*

6. Explain the following Big-O notation (please show us sample code in your favorite coding language)
    *  O(n<sup>2</sup>)
    *  O(2<sup>n</sup>)
    *  O(n)

7. Git: You are asked to work on a new existing project. When you browse the Git repo historical commits, you noticed many commits of small code changes and short description. Provide 3 advice you can give to the existing team to improve their Git practice?

8. Share with us one book that has changed your perspective in life. How did it change your life?

9. What is the thing you believe in that is true, that most people disagree with?

10. What are your thoughts on the subject of Arts and Humanities?

11. Do you require a visa to work in Malaysia?

# Simple Coding Assessment

Build a Cryptocurrency Market dashboard showing market data + Shopping Cart to buy them

**dashboard page**
* User can see a page list of 20 or more coins.
* User can see the sparkline (7 days) in the page list.
* User can click into each coin to see more details of the coin.
* User can switch currency.

**cart/checkout**

* User can select coin to put into cart.
* User can enter the quantity of a coin for a given line item in the cart.
* User can checkout the order in cart.
* User can see total bill, order history, with the quantity, price per unit, total price.

**reference**

* [https://www.coingecko.com/en/api](https://www.coingecko.com/en/api)
* [https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true](https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true)
* [https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true](https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true)
* [https://api.coingecko.com/api/v3/exchange_rates](https://api.coingecko.com/api/v3/exchange_rates)

**bonus**

* Host this on a website with a public URL.
* Showcase usage of cloud services for (managed database, redis/memcache etc.)
* User can favorite a coin from page list and from coin page.
* User can toggle the page list to show only favorite coins.

---
# Submission instruction

1. Fork this repo.
2. In your own git repo, create a new branch.
3. Once you are done, create a merge request (or pull request) against your master branch
4. Share the link and access to your gitlab repo in the job application email.

Note: Do not create Merge Request against this repository, else your submission will be automatically disqualified.
